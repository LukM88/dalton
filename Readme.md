# Dalton

## Opis

Repozytorium zawiera projekt na studia z systemów ekspertowych. Ma on badać wzrok pod kątem daltonizmu. 

## Obsługa

Skrypt `generator_kodu.py` to skrypt do generowania silnika logicznego aplikacji na podstawie plików konfiguracyjnych.Do działania programu konieczne są odpowiednie pliki konfiguracyjne:
- `rule.json` - zawiera regóły wykorzystywane do wnioskowania i wniosek
- `variables.json` - zawiera zmienne wykorzysywane w silniku do wnioskowania (wszystkie zmienne użyte w `rule.json` muszą znajdować się w tym pliku)