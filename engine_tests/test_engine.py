import os
import subprocess
import pytest
from src.utils import CodeGenerator

TEST_DATA_PREFIXES = [("low_probability.json", f"r3{os.linesep}"),
                      ("medium_probability.json", f"r2{os.linesep}"),
                      ("high_probability.json", f"r1{os.linesep}"),
                      ("no_dalton.json", f"r0{os.linesep}")]


@pytest.mark.parametrize("test_input, expected", TEST_DATA_PREFIXES)
def test_generated_file(test_input, expected):
    CodeGenerator(f"engine_tests/test_data/{test_input}",
                  "engine_tests/test_data/acquired_color_blindness_rules.json",
                  "engine_tests/test_data/conclusions.json").generate_script()
    test = subprocess.Popen(["python", "logic_engine.py", "--data-vector", "engine_tests/test_data/test_results.json"], stdout=subprocess.PIPE)
    assert test.communicate()[0].decode("utf8") == expected
