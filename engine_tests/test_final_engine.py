import json
import os
import subprocess
import pytest
from src.utils import CodeGenerator

with open('engine_tests/test_data/test_vectors.json') as f:
    TEST_DATA = json.load(f)


@pytest.mark.parametrize("test_vector", TEST_DATA)
def test_generated_file(test_vector):
    with open("results.json", "w") as f:
            f.write(json.dumps(test_vector['results']))
    CodeGenerator(conclusions_path="src/utils/configs/conclusions.json",
                rules_path="src/utils/configs/rules.json",
                variables_path="src/utils/configs/variables.json").generate_script()
    test = subprocess.Popen(["python", "logic_engine.py", "--data-vector", "results.json"], stdout=subprocess.PIPE)
    assert test.communicate()[0].decode("utf8") == test_vector['expected_result']+os.linesep
