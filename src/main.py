import os
import subprocess
import time
from utils import CodeGenerator
import json
from random import randint

COLOR_GROUPS = ["Red", "Yellow", "Blue", "Green", "Pink", "Purple", "Grey"]
MAX_SCORE = 20
TIME_FOR_TEST = 500
DALTON_SOURCES = ["psychoactive_drugs", "alcohol_overdosing", "eye_diseases", "retina_injury"]
POSSIBLE_ANSWERS = ['never', 'once', 'few times', 'many times']
PATH_TO_MULTIPLIERS = 'src/utils/configs/multipliers_vectors.json'
class DaltonTest:
    def __init__(self) -> None:
        with open("src/colors.json") as colors_file:
            self.colors = json.load(colors_file)
        with open("src/paterns.json") as patterns_file:
            self.patterns = json.load(patterns_file)
        self.pattern_color = ""
        self.palette = []
        self.scores = []
        self.colors_score = dict()
        for color_group in COLOR_GROUPS:
            self.colors_score[color_group] = 10

    def load_value(self, value):
        if value == 1:
            return f"\033[48;5;{self.colors[self.pattern_color]}m  \033[0;0m"
        return f"\033[48;5;{self.colors[self.palette[randint(0, len(self.palette)-1)]]}"\
               "m  \033[0;0m"

    def load_row(self, pattern, row_number):
        row = []
        for value in pattern[row_number]:
            row.append(self.load_value(value))
        return row

    def load_pattern(self, patterns):
        picture = []
        for row_number in range(0, len(patterns["coding"])):
            picture.extend(self.load_row(patterns["coding"], row_number))
            picture.append(f"{os.linesep}")
        return picture

    def append_score(self, new_list):
        self.scores.append(new_list)

    def run(self):
        start_time = time.time()
        is_done = False
        i = 0
        while True:
            color_group = COLOR_GROUPS[i]
            self.palette = []
            for color in self.colors.keys():
                if color_group in color:
                    self.palette.append(color)
            self.pattern_color = self.palette[randint(0, len(self.palette)-1)]
            self.palette.remove(self.pattern_color)
            pattern1 = self.patterns[randint(0, len(self.patterns)-1)]
            picture = self.load_pattern(pattern1)
            print("".join(picture))
            answer = input()
            if pattern1["value"] == answer[0]:
                self.colors_score[color_group] = int(self.colors_score[color_group]) + 1
            if TIME_FOR_TEST < time.time()-start_time \
                or MAX_SCORE in self.colors_score.values():
                    is_done = True
                    self.append_score(f"{self.colors_score}")
            if is_done:
                break
            i += 1
            if color_group == "Grey":
                i = 0
                self.append_score(f"{self.colors_score},")
        with open("results.json", "w") as f:
            f.write(json.dumps(self.colors_score))
        popen = subprocess.Popen(["python", "logic_engine.py", "--data-vector", "results.json"], stdout=subprocess.PIPE, universal_newlines=True)
        for stdout_line in iter(popen.stdout.readline, ""):
            print(stdout_line)
            result=stdout_line
        popen.stdout.close()
        return result

class DaltonProbability:
    
    def __init__(self) -> None:
        self.chance_for_dalton = 0.08
        with open(PATH_TO_MULTIPLIERS, 'r') as f:
            self.multipliers = json.load(f)
    def add_chance(self, chance):
        self.chance_for_dalton += self.gender_multiplier * chance
    
    def print_question(self, question):
        print(question)
        i = 0
        for answer in POSSIBLE_ANSWERS:
            print(f'{i}. {answer}')
            i += 1
        return int(input())

    def run_test(self):
        self.gender_multiplier = self.multipliers['gender'][int(input('What is your gender?[input answer number]\n 1. male 2. female'))]
        self.add_chance(self.multipliers['alcohol_overdosing'][self.print_question('How often do you overdose on alcohol? [input answer number]')])
        self.add_chance(self.multipliers['psychoactive_drugs'][self.print_question('How often do you take psychoactive drugs? [input answer number]')])
        self.add_chance(self.multipliers['eye_diseases'][self.print_question('How often do you have eye diseases [input answer number]')])
        self.add_chance(self.multipliers['retina_injury'][self.print_question('How often do you have retina injury? [input answer number]')])
    
    def print_results(self):
        print(f'''You may have {self.chance_for_dalton * 100}% chances to have color blindness spectrum''')
        print_disclaimer()

def print_disclaimer():
    print('''Test results are NOT scientifically accurate and should not be taken as diagnose
             If you have think you may be sick please contact specialist!''')

if __name__ == "__main__":
    CodeGenerator(conclusions_path="src/utils/configs/conclusions.json",
                rules_path="src/utils/configs/rules.json",
                variables_path="src/utils/configs/variables.json").generate_script()
    dalton_probability = DaltonProbability()
    dalton_probability.run_test()
    result = DaltonTest().run()
    if 'r0' not in result:
        dalton_probability.print_results
    else:
        print('You are ok')
        print_disclaimer()