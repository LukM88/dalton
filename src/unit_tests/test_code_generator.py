from genericpath import exists
from src.utils import CodeGenerator

SYNTAX_IN_SCRIPT = \
    "import argparse\n"\
    "import pathlib\n"\
    "import json\n"\
    "\n"\
    "def parse_arguments():\n"\
    "    parser = argparse.ArgumentParser()\n"\
    "    parser.add_argument('--data-vector', type=pathlib.Path, required=True,\n"\
    "                        help='Path to data vector file')\n"\
    "    return parser.parse_args()\n"\
    "\n"\
    "\n"\
    'if __name__ == "__main__":\n'\
    "    args = parse_arguments()\n"\
    '    input_data = None\n'\
    '    with open(args.data_vector, "r") as f:\n'\
    '        input_data = json.loads(f.read())\n'\
    '    var1 = 1\n'\
    '    var2 = 1\n'\
    '    var3 = 1\n'\
    '    if var1 == var2 and var2 == var3:\n'\
    '        print("r1")\n'\
    '    if var1 == var2 or var2 != var3:\n'\
    '        print("r2")\n'


def test_generated_file():
    CodeGenerator("src/unit_tests/test_configs/simple_test_variables.json",
                  "src/unit_tests/test_configs/simple_test_rules.json",
                  "src/unit_tests/test_configs/conclusions.json").generate_script()
    assert exists("logic_engine.py")
    assert open("logic_engine.py", "r").read() == SYNTAX_IN_SCRIPT
