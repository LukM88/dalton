import json


class CodeGenerator:

    def __init__(self, variables_path, rules_path, conclusions_path) -> None:
        with open("pattern.txt", "r") as f:
            self.syntax = f.read()
        self.tabulations = 1
        self.variables = json.load(open(variables_path))
        self.rules = json.load(open(rules_path))["rules"]
        for var in self.variables.keys():
            self.add_variable(var, self.variables[var])
        for rule in self.rules.keys():
            self.add_if(self.load_condition(self.rules[rule]),
                        f'print("{self.rules[rule]["conclusion"]}")')

    def add_variable(self, argument, value='None'):
        self.add_tabs()
        self.syntax += f'{argument} = {value}\n'

    def generate_script(self):
        script = open("logic_engine.py", "w")
        script.write(self.syntax)
        script.close()

    def add_tabs(self):
        for i in range(0, self.tabulations):
            self.syntax += "    "

    def add_if(self, condition, conclusion):
        self.add_tabs()
        self.syntax += f'if {condition}:\n'
        self.tabulations += 1
        self.add_tabs()
        self.syntax += f'{conclusion}\n'
        self.tabulations -= 1

    def load_condition(self, rule):
        i = 1
        condition = ""
        for confition_part in rule["conditions"].keys():
            if i % 2 == 1:
                condition += rule["conditions"][confition_part]
            else:
                condition += rule["conditions"][confition_part]
                i += 1
        return condition
